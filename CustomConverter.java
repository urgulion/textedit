/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textedit;

import com.google.gson.*;
import java.lang.reflect.Type;

/**
 *
 * @author valeria
 */
public class CustomConverter implements JsonSerializer<Note>, JsonDeserializer<Note>  {
    public JsonElement serialize(Note src, Type type,
            JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.addProperty("name", src.toString());
        object.addProperty("color", src.toString());
        object.addProperty("font", src.toString());
        object.addProperty("size", src.size.toString());
        object.addProperty("bold", src.toString());
        object.addProperty("italics", src.toString());
        object.addProperty("underline", src.toString());
        object.addProperty("strikeout", src.toString());
        return object;
    }

    public Note deserialize(JsonElement json, Type type,
            JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String name = new String(object.get("name").getAsString());
        String color = new String(object.get("color").getAsString());
        String font = new String(object.get("font").getAsString());
        Integer size = new Integer(object.get("size").getAsString());
        String bold = new String(object.get("bold").getAsString());
        String italics = new String(object.get("italics").getAsString());
        String underline = new String(object.get("underline").getAsString());
        String strikeout = new String(object.get("strikeout").getAsString());
        
        return new Note(name, color, font, size, bold,
                italics, underline, strikeout);
    }
}

