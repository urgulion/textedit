/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textedit;

import java.util.Random;

/**
 *
 * @author FilatovaUlyana
 */
public class Note {
    //private String message;
    //private TextStyle style;
    String name;
    String color;
    String font;
    Integer size;
    String bold;
    String italics;
    String underline;
    String strikeout;
    
    /*Note(String message, TextStyle style)
        {this.message = message;
         this.style = style;
        }*/
    public Note()
        {}
    
    public Note(String name, String color, String font, Integer size,
                String bold, String italics, String underline, String strikeout)
            {this.name = name;
             this.color = color;
             this.font = font;
             this.size = size;
             this.bold = bold;
             this.italics = italics;
             this.underline = underline;
             this.strikeout = strikeout;
            }
    public String getName ()
        {return name;
        }
    
    public String returnString (String text)
        {text = "<font face=\""+font+"\"> <font size=\""+size+"\"> <font color=\""+color+"\">" + text + "</font></font></font>";
        if (bold.equals("true"))
            {text = "<b>" + text + "</b>";
            }
        if (italics.equals("true"))
            {text = "<i>" + text + "</i>";
            }
        if (underline.equals("true"))
            {text = "<u>" + text + "</u>";
            }
        if (strikeout.equals("true"))
            {text = "<s>" + text + "</s>";
            }
         return text;
        }
    
    public Note generateNote (String name)
        {this.name = name;
        
        Random random = new Random();
        
        String colors[] = {"aqua", "black", "blue", "fuchsia", "gray",
                           "green", "lime", "maroon", "navy", "olive",
                           "purple", "red", "silver", "teal", "yellow"};
        String fonts[] = {"Tahoma", "Arial Black", "Times New Roman",
                          "Calibri", "Bookman Old Style", "Chiller"};
        
        size = random.nextInt(7) + 1;
        
        color = colors[random.nextInt(14)];
        font = fonts[random.nextInt(5)];
        
        bold = String.valueOf(random.nextBoolean());
        italics = String.valueOf(random.nextBoolean());
        underline = String.valueOf(random.nextBoolean());
        strikeout = String.valueOf(random.nextBoolean());
        return this;
        }
}
