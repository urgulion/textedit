/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textedit;

/**
 *
 * @author FilatovaUlyana
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
 
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;


public class TextEdit extends JFrame{
    private static final int AREA_WIDTH = 500;
    private static final int AREA_HEIGHT = 700;
    private static final TextStyle style = new TextStyle();
    private static Note note = new Note();
    private static String text = new String();
   
     
    public static void createGUI() {
         
        JFrame frame = new JFrame("Test frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       		
                
        Font font = new Font("Verdana", Font.PLAIN, 11);
         
        JMenuBar menuBar = new JMenuBar();
         
        JMenu fileMenu = new JMenu("File");
        fileMenu.setFont(font);
         
        JMenuItem newItem = new JMenuItem("New file");
        newItem.setFont(font);
        fileMenu.add(newItem);
        
        JMenuItem openItem = new JMenuItem("Open");
        openItem.setFont(font);
        fileMenu.add(openItem);
                                          
        JMenuItem saveItem = new JMenuItem("Save");
        saveItem.setFont(font);
	fileMenu.add(saveItem);
         
        fileMenu.addSeparator();
         
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.setFont(font);
        fileMenu.add(exitItem);
        
        JTextPane area = new JTextPane();
        area.setContentType("text/html");
        area.setLocation(50,70);
        area.setSize(AREA_WIDTH, AREA_HEIGHT);
        
        frame.add(area);
       
        /////////////////////////////////
        openItem.addActionListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e){
                JFileChooser Chooser = new JFileChooser();
                Chooser.showOpenDialog(frame);
                File file = Chooser.getSelectedFile();
            	  try (FileReader reader = new FileReader(file)){
                       char [] buf = new char [(int)file.length()];
			reader.read(buf);
                        //вызов поиска стиля в json 
                        note = style.findByName(file.getName());
                        //приминение стиля в зависимости от полученного
                        text = note.returnString(new String(buf));
                        area.setText(text);
				} catch (IOException ex) { 
                        Logger.getLogger(TextEdit.class.getName()).log(Level.SEVERE, null, ex);
                    } 
            	}                      
        });

        saveItem.addActionListener(new ActionListener() {
            @Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser save = new JFileChooser();
		int option = save.showSaveDialog(save);
		if (option == JFileChooser.APPROVE_OPTION) {
			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(save.getSelectedFile().getPath()));
				out.write(area.getText());
				out.close();
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
                }
        }
      }
    });
 
         newItem.addActionListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e){
              javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public  void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                createGUI();
                }
              });
            }
        });
            	                      
        exitItem.addActionListener(new ActionListener() {           
            public void actionPerformed(ActionEvent e) {
                System.exit(0);             
            }           
        });
        ////////////////////////////////
        exitItem.addActionListener(new ActionListener() {           
            public void actionPerformed(ActionEvent e) {
                System.exit(0);             
            }           
        });
         
        menuBar.add(fileMenu);
                 
        frame.setJMenuBar(menuBar);
         
        frame.setPreferredSize(new Dimension(270, 225));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    
}
    
