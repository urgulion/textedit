/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textedit;

import java.util.List;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 *
 * @author FilatovaUlyana
 */
public class TextStyle {
    
    private List<Note> notes = null;
    private GsonBuilder builder = new GsonBuilder();
    private Gson gson = new Gson();
    BufferedReader br = null;
    private static final String FILENAME = "C:\\Users\\valeria\\Documents\\styles.json";
    
    public TextStyle()
        {builder.registerTypeAdapter(Note.class, new CustomConverter());
         gson = builder.create();
         gson = new Gson();
         try 
            {br = new BufferedReader(new FileReader(FILENAME));
            Type type = new TypeToken<List<Note>>(){}.getType();
            notes = gson.fromJson(br, type);
            }
         catch (IOException e)
            {e.printStackTrace();
            }
         }
    
    public Note findByName (String name)
        {boolean tmp = false;
         int tmp2 = 0;
         for (int i=0; i<notes.size(); i++)
            {if (name.equals(notes.get(i).getName()))
                {tmp = true;
                 tmp2 = i;
                 break;
                }
            }
         if (tmp)
            {return notes.get(tmp2);
            }
         else
            {/*generate style
                add info into json
                return generated style*/
             notes.add(new Note().generateNote(name));
             
             try (FileWriter fileWriter = new FileWriter(FILENAME))
                {
                    fileWriter.write(gson.toJson(notes));
                }
             catch (IOException e)
                {e.printStackTrace();
                }
             
             return notes.get(tmp2);
            }            
        }
    
}
