/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textedit;

import javax.swing.JFrame;
import static textedit.TextEdit.createGUI;

/**
 *
 * @author FilatovaUlyana
 */
public class Main {
    public static void main(String[] args) {
        
         javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public  void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                createGUI();
            }
        });
    }
}
